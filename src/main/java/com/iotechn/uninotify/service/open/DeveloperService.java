package com.iotechn.uninotify.service.open;

import com.iotechn.uninotify.annotation.HttpMethod;
import com.iotechn.uninotify.annotation.HttpOpenApi;
import com.iotechn.uninotify.annotation.HttpParam;
import com.iotechn.uninotify.annotation.HttpParamType;
import com.iotechn.uninotify.annotation.param.NotNull;
import com.iotechn.uninotify.exception.ServiceException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/12/26
 * Time: 16:23
 */

@HttpOpenApi(group = "developer", description = "服务")
public interface DeveloperService {

    @HttpMethod(description = "获取用户绑定微信二维码URL，将其直接在前端生成二维码即可")
    public String getRegisterUrl(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.COMMON, description = "第三方用户Id") String userId) throws ServiceException;

    @HttpMethod(description = "查看指定用户是否被注册到系统")
    public Boolean isRegister(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.COMMON, description = "第三方用户Id") String userId) throws ServiceException;

}
